﻿DROP DATABASE IF EXISTS opcion2;
CREATE DATABASE IF NOT EXISTS opcion2;
USE opcion2;
CREATE TABLE ejemplar (
  cod_ejemplar int AUTO_INCREMENT,
  PRIMARY KEY (cod_ejemplar));
CREATE TABLE socio (
  cod_socio int AUTO_INCREMENT,
  PRIMARY KEY (cod_socio));
CREATE TABLE presta (
  fechaini date,
  fechafin date,
  cod_ejemplar int,
  cod_socio int,
  PRIMARY KEY (cod_ejemplar, cod_socio),
  UNIQUE KEY (cod_ejemplar),
  CONSTRAINT prestaejemplar FOREIGN KEY (cod_ejemplar) REFERENCES ejemplar (cod_ejemplar),
  CONSTRAINT prestasocio FOREIGN KEY (cod_socio) REFERENCES socio (cod_socio));