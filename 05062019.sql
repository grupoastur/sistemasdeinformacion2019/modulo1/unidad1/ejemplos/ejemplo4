﻿DROP DATABASE IF EXISTS b20190605;
CREATE DATABASE b20190605;
USE b20190605;

CREATE OR REPLACE TABLE personas (
dni varchar (10),
  nombre varchar (50),
  PRIMARY KEY (dni)
);

INSERT INTO personas (dni, nombre)
  VALUES 
  ('dni1', 'nombre1'),
  ('dni2','nombre2'),
  ('dni3','nombre3');

CREATE OR REPLACE TABLE telefonos (
  dni varchar(10),
  numero varchar(12),
  PRIMARY KEY (dni, numero),
  CONSTRAINT fktelefonospersonas FOREIGN KEY (dni) REFERENCES personas (dni)
);
INSERT INTO telefonos (dni, numero)
  VALUES ('dni1', 'tel1'),
('dni1', 'tel2'),
('dni3', 'tel2'),
('dni1' , 'tel3')
;






