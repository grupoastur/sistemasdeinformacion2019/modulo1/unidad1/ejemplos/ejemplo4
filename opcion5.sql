﻿DROP DATABASE IF EXISTS opcion5;
CREATE DATABASE IF NOT EXISTS opcion5;
USE opcion5;
CREATE TABLE socio (
  cod_socio int AUTO_INCREMENT,
  PRIMARY KEY (cod_socio));
CREATE TABLE ejemplar (
  cod_socio int,
  cod_ejemplar int AUTO_INCREMENT,
  fechaini date,
  fechafin date,
  PRIMARY KEY (cod_ejemplar),
  CONSTRAINT ejemplarsocio FOREIGN KEY (cod_socio) REFERENCES socio (cod_socio)); 
