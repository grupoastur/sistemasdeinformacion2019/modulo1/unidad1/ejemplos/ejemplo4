﻿DROP DATABASE IF EXISTS opcion6;
CREATE DATABASE IF NOT EXISTS opcion6;
USE opcion6;
CREATE TABLE ejemplar (
  cod_ejemplar int AUTO_INCREMENT,
  PRIMARY KEY (cod_ejemplar));
CREATE TABLE socio (
  cod_ejemplar int,
  cod_socio int AUTO_INCREMENT,
  fechaini date,
  fechafin date,
  PRIMARY KEY (cod_socio),
  CONSTRAINT socioejemplar FOREIGN KEY (cod_ejemplar) REFERENCES ejemplar(cod_ejemplar)
  );